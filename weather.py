from flask import Flask,jsonify
import requests
import json
import boto3
from datetime import datetime
import os
import sys
import time

app = Flask(__name__)

def api_call_weather_data(api_key, lat, lon):
    #  API https://openweathermap.org/api/one-call-3#current
    #  Free API only current, exlude minutely,hourly,daily,alerts
    #  Price https://openweathermap.org/price
    url = f"https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&exclude=minutely,hourly,daily,alerts&appid={api_key}"
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Failed to download data. Status code: {response.status_code}")
        return None

def sort_weather_data(data):

    location_timezone = data.get('timezone')
    location_lat = data.get('lat')
    location_lon = data.get('lon')
    current_weather = data.get('current', {})
    weather_description = current_weather.get('weather', [{}])[0].get('description')
    temperature = current_weather.get('temp')
    humidity = current_weather.get('humidity')
    wind_speed = current_weather.get('wind_speed')

    return {
        'Location location_timezone': location_timezone,
        'LAT': location_lat,
        'LON': location_lon,
        'Description': weather_description,
        'Temperature': temperature,
        'Humidity': humidity,
        'Wind Speed': wind_speed
    }


def upload_to_s3(data, bucket_name, file_name):
    access_key = os.environ.get('AWS_ACCESS_KEY_ID')
    secret_key = os.environ.get('AWS_SECRET_ACCESS_KEY')
    session_token = os.environ.get('AWS_SESSION_TOKEN')
    s3 = boto3.client('s3', aws_access_key_id=access_key, aws_secret_access_key=secret_key, aws_session_token=session_token)
    s3.put_object(Body=json.dumps(data), Bucket=bucket_name, Key=file_name)


@app.route('/health')
def health_check():
    # Health check endpoint
    return jsonify({'status': 'ok'})

@app.route('/upload-data')
# if __name__ == "__main__":
def send_weather():
    # Set your OpenWeatherMap API key
    api_key = os.environ.get('API_OPENWEATHERMAP_KEY')
    bucket_name = os.environ.get('AWS_S3_BUCKET_NAME')

    # For test, used with sleep
    # while True:

    # Load city data from JSON file
    with open('./city-list.json', 'r') as file:
        cities = json.load(file)
    
    # Loop city from JSON file
    weather_data = {}
    for city in cities:
        city_name = city['name']
        country = city['country']
        lat = city['coord']['lat']
        lon = city['coord']['lon']
        
        # Download weather data
        clear_weather_data = api_call_weather_data(api_key, lat, lon)
        if clear_weather_data:
        # Extract specific weather data
            sorted_extracted_data = sort_weather_data(clear_weather_data)
            weather_data[city_name] = sorted_extracted_data
            # print(clear_weather_data)
            # Upload data to AWS S3
            file_name = f"{city_name}_{country}_{datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.json"
            # upload_to_s3(clear_weather_data, bucket_name, file_name)
            print(f"Weather data for {city_name}, {country} uploaded to S3. File name: {file_name}")

    return jsonify(weather_data)
    # Set the interval for data download hourly
    # time.sleep(3600)



if __name__ == "__main__":
    app.run(debug=False, host='0.0.0.0', port='8080')
            