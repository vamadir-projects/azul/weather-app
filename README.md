# weather-app

Weather app download data over free api from openweather

# Function sort_weather_data(data)
Extracting specific data relevant to the current weather condition

```
JSON EXAMPLE
    'lat': 44.804, 
    'lon': 20.4651, 
    'timezone': 'Europe/Belgrade', 
    'timezone_offset': 3600, 
    'current': 
        { 
        'dt': 1707854139, 
        'sunrise': 1707802978, 
        'sunset': 1707840117, 
        'temp': 280.91, 
        'feels_like': 277.15, 
        'pressure': 1015, 
        'humidity': 80, 
        'dew_point': 277.68, 
        'uvi': 0, 
        'clouds': 20, 
        'visibility': 10000, 
        'wind_speed': 7.2, 
        'wind_deg': 310, 
        'weather': [{ 
                'id': 801, 
                'main': 'Clouds', 
                'description': 'few clouds', 
                'icon': '02n'} 
```